<?php require_once "./code.php" ; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S2 Activity</title>
</head>
<body>


	<h3>Loops</h3>
	<?php divisible() ;?>




	<h3>Array Manipulation</h3>
	<?php $students = [] ; ?>
	<?php array_push($students, 'Jeremy') ; ?>
	<?php array_push($students, 'Kristin') ; ?>
	<?php array_push($students, 'Grace') ; ?>
	<?php array_push($students, 'Mikaila') ; ?>
	<?php array_push($students, 'Jana') ; ?>
	<?php print_r($students) ; ?>
	</br>
	<?= count($students) ; ?>
	</br>
	<?php array_push($students, 'Flor') ; ?>
	<?php print_r($students) ; ?>
	<?= count($students) ; ?>
	</br>
	<?php array_shift($students) ; ?>
	<?php print_r($students) ; ?>
	<?= count($students) ; ?>
</body>
</html>